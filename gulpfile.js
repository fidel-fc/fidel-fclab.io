var gulp = require('gulp'),
    cleanCSS = require('gulp-clean-css'),
    pngmin = require('gulp-pngmin'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify-es').default,
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    htmlmin = require('gulp-htmlmin'),
    svgmin = require('gulp-svgmin');






gulp.task('svgmin', function () {
    return gulp.src('src/img/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest('src/img'));
});


gulp.task('htmlmin', function () {
    return gulp.src('src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('public/'));
});

gulp.task('pngmin', function () {
    gulp.src(['src/img/**/*.*', '!src/img/sprites/**/*'])
        .pipe(pngmin())
        .pipe(gulp.dest('./public/img'));
});

gulp.task('minify-css', ['compile-sass'], function () {
    return gulp.src('./public/css/*.css')
        .pipe(cleanCSS())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('concat', function () {
    return gulp.src([
        "src/js/*.js"
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('uglify', ['concat'], function () {
    return gulp.src('./public/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'));

});

gulp.task('compile-sass', function () {
    return gulp.src('./src/scss/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('dist', ['minify-css', 'uglify'], function () {
});
